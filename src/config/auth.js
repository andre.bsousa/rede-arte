import jwt from 'jsonwebtoken'

const TOKEN_KEY = 'rede-arte'

const getToken = () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY))
    if(data && data.token){
        return data.token
    }
    return false
}

const getUser = () => {
    if(getToken()){
    const { user } = jwt.decode(getToken())
    if(user){
        return user
    }
}
    return false
}

const isAdmin = () => {
    const { is_admin } = getUser()
    return is_admin
}

const saveLocalStorage = (data) => localStorage.setItem(TOKEN_KEY, JSON.stringify(data))

const removeToken = () => localStorage.removeItem(TOKEN_KEY)

const isAtuthenticated = () => {
    // pegar dentro do local storage
    // validar o token
    // retornar se true ou false
    return getToken() !== false
}

export {
    getToken,
    getUser,
    isAdmin,
    saveLocalStorage,
    removeToken,
    isAtuthenticated
}