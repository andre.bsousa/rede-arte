import { http } from '../config/http'

const getUsersService = (data) => http.get('/user', data)

export {
    getUsersService
}