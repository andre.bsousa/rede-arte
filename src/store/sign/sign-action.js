import { saveLocalStorage, isAdmin } from "../../config/auth"
import history from "../../config/history"
import { http } from "../../config/http"
import { authentication } from "../../services/auth-service"
import { toastr } from 'react-redux-toastr'

export const SIGN = "SIGN"
export const SIGN_LOADING = "SIGN_LOADING"

export const signIn = (props) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SIGN_LOADING, loading: true })
      const { data } = await authentication(props)
      dispatch({ type: SIGN, payload: data })
      http.defaults.headers["x-auth-token"] = data.token
      saveLocalStorage(data)
      isAdmin ? history.push("/admin") : history.push("/profile")
    } catch (error) {
      toastr.error("ERROR !", `Não foi possível fazer o login, verifique login e senha`)

    }
  }
}