const INITIAL_STATE = {
    users: []
}

export const reducer = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case 'GET_ALL_USERS':
            state.users = action.payload
            return state
        case 'CREATE_USER':
            return state
        default:
            return state
    }

}