import { getUsersService } from '../../services/user-service'

export const userList = () => {
    return async (dispatch) => {
        const res = await getUsersService()
        dispatch({
            type: 'GET_ALL_USERS',
            payload: res.data
        })
    }
}