import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import signReducer from './sign/sign-reducer'
import { reducer as UserReducer } from "./user/user-reducer"
import { reducer as toastrReducer } from "react-redux-toastr"
import thunk from 'redux-thunk'
import multi from "redux-multi"

const reducers = combineReducers({
    auth: signReducer,
    user: UserReducer,
    toastr: toastrReducer
})

const middlewares = [thunk, multi]

const compose = composeWithDevTools(
    applyMiddleware(...middlewares)
    )

const store = createStore(
    reducers,
    compose
)


export default store