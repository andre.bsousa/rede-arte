import React, { useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap'
import { useDispatch } from "react-redux"
import { signIn } from "../../store/sign/sign-action"
import styled from "styled-components"


export const AuthForm = () => {

        const dispatch = useDispatch()
        const [form, setForm] = useState({})
        const [loading, setLoading] = useState(false)
    
        const handleChange = (attr) => {
            setForm({
                ...form,
                [attr.target.name]: attr.target.value
            })
        }
    
        const isSubmitValid = () => form.email && form.password

        const submitLogin = () => {
            dispatch(signIn(form))
            return
          }        

    return (
        <div>
            <Form.Group>
                <Form.Control className="formEmail" onChange={handleChange} type="email" name="email" placeholder="Insira seu email" value={form.email || ""} />
            </Form.Group>

            <Form.Group>
                <Form.Control className="formPassword" onChange={handleChange} type="password" name="password" placeholder="Insira sua senha" value={form.password || ""} />
            </Form.Group>

            <Button onClick={submitLogin} disabled={!isSubmitValid()} className={`btn ${isSubmitValid() ? 'btn-primary' : "btn-secondary"} btn-user btn-block`} variant="primary" type="submit">

                {loading ? (
                    <>
                        <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />
                        Carregando...
                    </>
                ) : "Login"}

            </Button>
        </div>
    )
}