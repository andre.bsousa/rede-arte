import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { userList } from '../../store/user/user-action'
import UserList from './user-list'

const UserFetch = () => {

    const dispatch = useDispatch()
    const users = useSelector(state => state.user.users)

    useEffect( () => {
        dispatch(userList())
    }, [dispatch])

    /*const _deleteProduct = async (obj) => {
  
        const result = await resultConfirmation('Produto', obj)

        if(result.isConfirmed){
            dispatch(deleteProduct({ product: obj }))
        }
    }*/

    return (
        <div>
        <UserList users={users} />            
        </div>
    )
}

export default UserFetch
