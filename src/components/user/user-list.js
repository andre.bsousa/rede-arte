import React from 'react'
import { Button, Table } from 'react-bootstrap'
import styled from 'styled-components'
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa'


const UserForm = ({users}) => {
    return (
        <div>
            <NewTable striped hover size="sm">
                <thead>
                    <tr>
                        <THeadItem>Nome</THeadItem>
                        <THeadItem></THeadItem>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => (
                        <tr key={user._id}>
                            <TbodyItem>{user.name}</TbodyItem>
                            <TbodyItem>
                                <ActionButton onClick='' variant="link" size="sm">
                                    <FaRegEdit />                                </ActionButton>
                                <ActionButton onClick='' variant="link" size="sm">
                                    <FaTrashAlt />
                                </ActionButton>
                            </TbodyItem>
                        </tr>
                    ))}

                </tbody>
            </NewTable>

        </div>
    )
}

export default UserForm

const NewTable = styled(Table)`
font-size: 14px
`

const THeadItem = styled.th`
background: #666;
color:#eee;
`
const TbodyItem = styled.td`
:nth-child(1){  width: 10%; }
:nth-child(2){  width: 10%; }
:nth-child(3){  width: 20%; }
:nth-child(4){  width: 10%; }
:nth-child(5){  width: 10%; }
:nth-child(6){  width: 10%; }
:nth-child(7){  width: 10%; }
:nth-child(8){  width: 20%; }
`
const ActionButton = styled(Button)`
padding:2px 4px;
font-weight:500;
font-size: 16px;

:hover {
    opacity:0.4;
    color: red
}
`