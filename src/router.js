import { Redirect, Route, Router, Switch } from 'react-router-dom'
import history from './config/history'
import { AuthForm } from './components/auth/auth-form'
import { isAtuthenticated, isAdmin } from './config/auth'
import UserFetch from './components/user/user-fetch'


const Routers = () => {

    const AuthRoute = ({ ...rest }) => {
        if (!isAtuthenticated()) {
            return <Redirect to='/' />
        }
        return <Route {...rest} />
    }

    const AdminRoute = ({ ...rest }) => {
        if (isAtuthenticated() && !isAdmin()) {
            return <Redirect to='/profile' />
        }
        return <Route {...rest} />
    }

    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={AuthForm} />
                <AuthRoute exact path="/profile" component={ () => <h1>Aqui ta as artes</h1>} />
                <AdminRoute exact path="/admin" component={UserFetch} />
            </Switch>
        </Router>
    )
}

export default Routers